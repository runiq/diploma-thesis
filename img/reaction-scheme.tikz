% ! TEX root = ../main.tex

\schemestart
  \begin{tikzpicture}[chemfig]
    \node[draw=none,anchor=north west] (gsh_donor) at (-0.25,1.6) {\vflipnext\chemfig[transparent][transparent]{!{gsh_short}}};
    \node[anchor=north west] at (0.01,0.95) {\chemfig{!{thiol}}};
    \node[anchor=south west] at (0.6,0) {\chemfig{!{imi}}};
    \node[anchor=south east] at (1.9,0.01) {\chemfig{!{aspsc}}};
    \arcThroughThreePoints[enzymebg]{pcs_cys_ori}{pcs_his_ori}{pcs_asp_ori}
  \end{tikzpicture}
  \arrow{-X>[\sffamily\footnotesize\acs*{GSH}][][][][][\sffamily\footnotesize 1]}
  \begin{tikzpicture}[chemfig]
    \node[anchor=north west] at (-0.25,1.6) {\vflipnext\chemfig{!{gsh_short}}};
    \node[anchor=north west] at (0.01,0.95) {\chemfig{!{thiol}}};
    \node[anchor=south west] at (0.6,0) {\chemfig{!{imi}}};
    \node[anchor=south east] at (1.9,0.01) {\chemfig{!{aspsc}}};
    \draw[hbond] (imi_hasp) -- (asp_o);
    \arcThroughThreePoints[enzymebg]{pcs_cys_ori}{pcs_his_ori}{pcs_asp_ori}
    \begin{scope}[elmovements]
      \draw (cys_bond) .. controls +(45:0.2in) and +(45:0.2in) .. (cys_s);
      \draw (imi_ncys) .. controls +(180:0.2in) and +(-90:0.2in) .. (cys_h);
    \end{scope}
  \end{tikzpicture}
  \arrow{->[*{0}\sffamily\footnotesize 2]}[-90]
  \begin{tikzpicture}[chemfig]
    \node[anchor=north west] at (-0.25,1.6) {\vflipnext\chemfig{!{gsh_short}}};
    \node[anchor=north west] at (0.01,0.95) {\vflipnext\chemfig{!{thiol_deprot}}};
    \node[anchor=south west] at (0.35,0) {\chemfig{!{imi_prot}}};
    \node[anchor=south east] at (1.9,0.01) {\chemfig{!{aspsc}}};
    \draw[hbond] (imi_hasp) -- (asp_o);
    \arcThroughThreePoints[enzymebg]{pcs_cys_ori}{pcs_his_ori}{pcs_asp_ori}
    \begin{scope}[elmovements]
      \draw (cys_s) .. controls +(0:0.2in) and +(-90:0.2in) .. (gsh_c);
      \draw (gsh_bond) .. controls +(0:0.2in) and +(0:0.2in) .. (gsh_o);
    \end{scope}
  \end{tikzpicture}
  \arrow{->[*{0}\sffamily\footnotesize 3]}[-90]
  \begin{tikzpicture}[chemfig]
    \node[anchor=north west] at (-0.25,1.6) {\chemfig{!{acyl_pcs_trans}}};
    \node[anchor=south west] at (0.35,0) {\chemfig{!{imi_prot}}};
    \node[anchor=south east] at (1.9,0.01) {\chemfig{!{aspsc}}};
    \draw[hbond] (imi_hasp) -- (asp_o);
    \arcThroughThreePoints[enzymebg]{acyl_pcs_ori}{pcs_his_ori}{pcs_asp_ori}
    \begin{scope}[elmovements]
      \draw (acyl_pcs_o) .. controls +(0:0.2in) and +(0:0.2in) .. (acyl_pcs_obond);
      \draw (acyl_pcs_nbond) .. controls +(-120:0.2in) and +(90:0.2in) .. (imi_hcys);
      \draw (imi_bondcys) .. controls +(60:0.2in) and +(60:0.2in) .. (imi_ncys);
    \end{scope}
  \end{tikzpicture}
  \arrow{-X>[*{0.base}\sffamily\footnotesize\acs*{GSH}][*{0.base}\sffamily\footnotesize Glycine][][][][\sffamily\footnotesize 4][][-0.25][-60]}[-180,1.2]
  \begin{tikzpicture}[chemfig]
    \node[anchor=west] at (0.75,1.2) {\chemfig{!{gsh_short2}}};
    \node[anchor=north west] at (-0.25,1.6) {\chemfig{!{acyl_pcs}}};
    \node[anchor=south west] at (0.6,0) {\chemfig{!{imi}}};
    \node[anchor=south east] at (1.9,0.01) {\chemfig{!{aspsc}}};
    \draw[hbond] (imi_hasp) -- (asp_o);
    \arcThroughThreePoints[enzymebg]{acyl_pcs_ori}{pcs_his_ori}{pcs_asp_ori}
    \begin{scope}[elmovements]
      \draw (acyl_pcs_obond) .. controls +(0:0.2in) and +(0:0.2in) .. (acyl_pcs_o);
      \draw (imi_ncys) .. controls +(120:0.2in) and +(-120:0.2in) .. (gsh_h);
      \draw (gsh_hbond) .. controls +(135:0.2in) and +(-45:0.2in) .. (acyl_pcs_c);
    \end{scope}
  \end{tikzpicture}
  \arrow{->[*{0}\sffamily\footnotesize 5]}[90]
  \begin{tikzpicture}[chemfig]
    \node[anchor=north west] at (-0.25,1.6) {\chemfig{!{acyl_pcs_trans2}}};
    \node[anchor=south west] at (0.35,0) {\chemfig{!{imi_prot}}};
    \node[anchor=south east] at (1.9,0.01) {\chemfig{!{aspsc}}};
    \draw[hbond] (imi_hasp) -- (asp_o);
    \arcThroughThreePoints[enzymebg]{acyl_pcs_ori}{pcs_his_ori}{pcs_asp_ori}
    \begin{scope}[elmovements]
      \draw (acyl_pcs_o) .. controls +(0:0.2in) and +(0:0.2in) .. (acyl_pcs_obond);
      \draw (acyl_pcs_sbond) .. controls +(0:0.2in) and +(90:0.2in) .. (imi_hcys);
      \draw (imi_bondcys) .. controls +(60:0.2in) and +(60:0.2in) .. (imi_ncys);
    \end{scope}
  \end{tikzpicture}
  \arrow{-X>[][*{0}\sffamily\footnotesize\ch{PC1}][*{0}\sffamily\footnotesize 6]}[90]
\schemestop

% vim: set filetype=tex:
