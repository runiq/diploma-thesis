%! TEX root = ../main.tex

\chapter{Appendix}
\label{cha:appendix}

\section{\emph{Modi Operandi} of Modeling Servers}
\label{sec:appendix_modi_operandi_of_modeling_servers}

In this \lcnamecref{sec:appendix_modi_operandi_of_modeling_servers}, the workings of all used modeling servers are described in some detail.

\subsection{HHpred}
\label{sub:appendix_hhpred}

Taken from \autocite{Soeding.etal2005}.

\begin{enumerate}
  \item HHsearch to find suitable templates:
    \begin{enumerate}
      \item Creation of \acp{HMM} of the query sequence by finding similar sequences using HHblits or \program{Psi-Blast}
      \item Comparing query \ac{HMM} to \acp{HMM} generated for multiple structural databases
    \end{enumerate}
    \item HHsearch also finds remotely homologous templates (since structural similarity is much more conserved than sequence similarity, these might still be good as templates)
    \item The multiple alignment created by HHsearch is then fed into \program{Modeller}
\end{enumerate}

\subsection{\program{I-Tasser}}
\label{sub:appendix_i-tasser}

Taken from \autocite{Roy.etal2010,Roy.etal2011}.

\begin{enumerate}
  \item \program{Lomets}, a threading meta-server, finds suitable templates and threads query sequence onto target structure
  \item \program{Lomets} generates spacial restraints for further use by \program{I-Tasser} by structural alignment of conserved sequences
  \item Building a structural model:
    \begin{itemize}
      \item Aligned fragments are assembled directly
      \item Non-aligned fragments are built using \abinitio{} modeling
    \end{itemize}
  \item Minimization using the \program{I-Tasser} force field, which incorporates:
    \begin{itemize}
      \item Hydrogen bonds
      \item Knowledge-based energy terms
      \item Sequence-based contact predictions (using \program{Svmseq})
      \item Spatial restraints collected in step 2
    \end{itemize}
  \item Creation of decoys
  \item Clustering of decoys to identify possible native folds (using \program{Spicker})
  \item Creation of representative cluster centroids (only \ch{C\Chemalpha} and side-chain center of mass)
  \item Repeat steps 3 to 8 twice
  \item Creation of full atomic model from cluster centroids
  \item Refinement of full atomic model by fragment-guided \ac{MD}
    \begin{enumerate}
      \item H-bond networks
      \item Backbone torsion angles
      \item Bond lengths
      \item Side-chain rotamer orientations
    \end{enumerate}
\end{enumerate}

\subsection{\program{Raptor-X}}
\label{sub:appendix_raptor-x}

Taken from \autocite{Peng.Xu2011}.

\begin{enumerate}
  \item \program{Psi-Blast} and HHpred to identify homologs, then regression tree-based scoring function to create sequence profile
  \item \program{Neff} to evaluate sparsity of alignment: The sparser it is, the more valued is structural information over sequence alignment
  \item \program{Neff} is also used to decide whether to use position-specific of context-specific gap penalty (context-specific depends on:
    \begin{itemize}
      \item Predicted secodary structure type
      \item Predicted solvent accessibility
      \item Amino acid identity
      \item Hydropathy count
      \item Buriedness)
    \end{itemize}
  \item Ranking of all templates by predicted query-template alignment
    \begin{itemize}
      \item Neural network-based approach
      \item Quality of an alignment is defined as the TM-score of the 3D model built from this alignment using \program{Modeller}
    \end{itemize}
  \item Selection of mutually similar templates:
    \begin{enumerate}
      \item Exclusion of all but top 20 templates
      \item Exclusion of templates with quality $ \geq \SI{10}{\percent}$ less than highest rated template
      \item Exclusion of templates whose TM-score with first-ranked template $< 0.65$
    \end{enumerate}
    \item If there are no templates left, go to last step
    \item If there are $\geq 2$ template left: Creation of initial \acp{PAM} for target-template pairs (\iac{PAM} encodes all possible alignments between two sequences)
    \item Creation of structural alignment \acp{PAM} using TMalign and/or \program{Matt}
    \item Iterative adjustment of to maximize consistency among all \acp{PAM} (i.e. to improve alignment quality)
    \item Create 3D models from alignment(s) using \program{Modeller}
\end{enumerate}

\subsection{\Phyre}
\label{sub:appendix_phyre}

Taken from the supplementary material to \autocite{Bennett-Lovsey.etal2008}.

\begin{enumerate}
  \item HHsearch to find close and remote sequence homologs
  \item Prediction of query secondary structure by taking the average confidence score of Psi\program{Pred}, SSPro and JNet prediction methods
  \item Disorder prediction using Disopred
  \item Profile creation by combining secondary structure prediction and sequence alignment
  \item Profile is scanned against fold library, which is constructed in the following way:
    \begin{enumerate}
      \item Sequences of structures deposited in \database{PDB} and \database{SCOP}\ldots
      \item \ldots are scanned against nonredundant sequence database
      \item Profile is constructed and deposited in \enquote{fold library}
      \item Known and predicted secondary structures are also incorporated in those profiles
    \end{enumerate}
  \item Alignment process returns a score on which which alignments are ranked
  \item Computation of E-values of ranked alignments
  \item Top ten highest scoring alignments are used to create full 3D models
  \item Missing/inserted regions are repaired using a loop library and reconstruction procedure
  \item Side chains are placed on the model using a fast graph-based algorithm and side chain rotamer library
\end{enumerate}

\subsection{\program{Multicom}}
\label{sub:appendix_multicom}

Taken from \autocite{Wang.etal2010}.

\begin{enumerate}
  \item Template identification by three methods and search against in-house databases:
    \begin{itemize}
      \item \program{Psi-Blast} (\ac{PSSM})
      \item HHsearch (\acp{HMM})
      \item \program{Compass} (profile)
      \item Sensitive machine learning fold recognition method \autocite{Cheng.Baldi2006}
    \end{itemize}
  \item Ranking of templates according to E-value
  \item Alignment of query to top 10 templates using \program{Spem}
  \item Template combination in order to improve performance \autocite{Cheng2008}:
    \begin{enumerate}
      \item Query aligned to top template and other templates with E-value $\leq$ \numrange{10}{20} and $\geq$ \SI{75}{\percent} coverage)
      \item Fragments from alignments below E-value/coverage threshold
      \item Removal of top template, rinse and repeat
      \item[→] Generation of up to 10 multiple alignments
    \end{enumerate}
  \item Model generation
    \begin{itemize}
      \item Models with \ac{QTA}:
        \begin{enumerate}
          \item \program{Modeller} 7v7 to create 10 models
          \item Model with minimum \program{Modeller} energy is returned as predicted model
        \end{enumerate}
        \item Models without \ac{QTA}:
          \begin{enumerate}
            \item \program{Rosetta} to create 200 models
            \item Clustering by \program{Rosetta}
            \item Centroids of several large clusters are returned as predicted models
          \end{enumerate}
    \end{itemize}
  \item Model evaluation using ModelEvaluator; comparison of several model parameters with values predicted from sequence by means of the \program{Scratch} suite
    \begin{itemize}
      \item Secondary structure
      \item Solvent accessibility
      \item Contact map
      \item \Chembeta-sheet topology
    \end{itemize}
  \item Calculation of \score{Gdt\_ts} using \iac{SVM} approach
  \item Ranking of predicted models using \score{Gdt\_ts}
\end{enumerate}

\subsection{\program{Swiss-Model}}
\label{sub:appendix_swiss-model}

Taken from \autocite{Schwede.etal2003}.

\begin{enumerate}
  \item Creation of \database{ExPDB} database from \database{PDB}
    \begin{enumerate}
      \item Removal of theoretical models
      \item Removal of low quality and \ch{C\Chemalpha} trace structures
      \item Addition of information for template selection to \database{PDB} header (e.g. probably quaternary structure, quality indicators such as empirical force field energy, \program{Anolea} mean force potential scores)
    \end{enumerate}
  \item Template selection by \program{Psi-Blast}ing query sequence against sequences in \database{ExPDB} database
    \begin{itemize}
      \item If templates cover distinct regions of target sequence (i.e. query has multiple domains), modeling process will be split into separate independent batches
    \end{itemize}
  \item Superposition of up to 5 template structures using an iterative least squares algorithm
  \item Creation of structural alignment after removing incompatible templates (i.e. omitting structures with high \ch{C\Chemalpha}-\ac{RMSD} to first template)
  \item Local pairwise alignment of target sequence to main template structures
    \begin{itemize}
      \item Placement of insertions and deletions optimized considering template structure context
    \end{itemize}
  \item Backbone atom positions of template structure are averaged
    \begin{itemize}
      \item[→] Templates weighted by sequence similarity to target sequence
      \item Exclusion of significantly deviating atom positions
    \end{itemize}
  \item Construction of non-aligned regions using constraint space programming
    \begin{enumerate}
      \item Best loop selected using a scoring scheme, which accounts for:
        \begin{itemize}
          \item Force field energy
          \item Steric hindrance
          \item Favorable interactions (i.e. hydrogen bond formation)
        \end{itemize}
      \item If no suitable loop can be identified, flanking residues are included to allow for more flexibility
      \item For loops > 10 residues: Selection of loop conformation from loop library derived from experimental structures
    \end{enumerate}
  \item Side chain modeling based on weighted positions of corresponding residues in template structures
    \begin{enumerate}
      \item Starting with conserved residues
      \item Iso-sterical replacement with template structure side chains
      \item Possible side chain confirmations are selected from backbone dependent rotamer library
      \item Scoring function (accounts for hydrogen bonds, disulfide bridges) applied to select most likely conformation
    \end{enumerate}

  \item Energy minimization using \forcefield{GROMOS96} force field
\end{enumerate}

\subsection{Geno3D}
\label{sub:appendix_geno3d}

Taken from \autocite{Combet.etal2010}.

\begin{enumerate}
  \item \program{Psi-Blast} against \database{PDB} entries to find suitable template and generate \iac{QTA}
  \item User selects templates from list
  \item Secondary structure is predicted for query and templates
  \item Secondary structure information is used to recompute \ac{QTA}
  \item Calculation of distance and dihedral angle restraints from \ac{QTA}
  \item Statistical restraints are used for gaps
  \item Restraints are used as input for \program{Cns} software
  \item \program{Cns} produces models that fit these restraints as well as possible
\end{enumerate}

\subsection{\program{Loopp}}
\label{sub:appendix_loopp}

Taken from \autocite{Vallat.etal2008,Vallat.etal2009}.

\begin{enumerate}
  \item Phase I—template identification, using:
    \begin{itemize}
      \item Site-specific residue frequency profile (\program{Psi-Blast})
      \item Raw sequence and profile of the template generated from target profile database
      \item Template secondary structure (\program{Sable} [query]/\program{Dssp} [target])
      \item Exposed surface area of template (\program{Sable} [query]/\program{Dssp} [target])
      \item \program{Thom2} contacts between structural sites
      \item[→] Combination of those factors to obtain 20 \enquote{features}
    \end{itemize}
  \item Evaluation of all 20 features with all known protein structures in the database
  \item Tree-based approach that returns database hits from one search branch and performs remaining search branches on the remaining entries template database:
    \begin{enumerate}
      \item \program{Psi-Blast} with 3 iterations:
        \begin{enumerate}
          \item Rank hits according to SC and return top scoring hits
          \item Remove hits from search database
        \end{enumerate}
      \item Comparison of all features
        \begin{enumerate}
          \item Rank hits according to SC and return top scoring hits
          \item Remove hits from search database
        \end{enumerate}
      \item Exactly the same as previous branch
        \begin{enumerate}
          \item Rank hits according to SC and return top scoring hits
          \item Remove hits from search database
        \end{enumerate}
      \item Comparison of same 20 features, transformed to a uniform distribution
        \begin{enumerate}
          \item Rank hits according to SC and return top scoring hits
          \item Remove hits from search database
        \end{enumerate}

      \item Comparison of a quadratic expansion of 12 uniformly distributed features
        \begin{enumerate}
          \item Rank hits according to SC and return top scoring hits
        \end{enumerate}
    \end{enumerate}
  \item Create \ac{QTA} using \program{Ssaln} and build models using \program{Modeller}
  \item Clustering and ranking of returned hits according to SC and TM-score
  \item Phase II—template modeling. Repeat previous branches 0, 1, and 2 (the last one five times), but also use the following measures for creating scores:
    \begin{itemize}
      \item Z-score (more sensitive, but \numrange{100}{1000} times more computationally intensive)
      \item \program{Eneall} (all-atom potential)
      \item \program{Te13} (residue-based contact potential)
      \item \program{Fready} (coarse-grained potential for pair interactions)
      \item \program{Sift} (assessment score; includes multiple measures; see \autocite[5]{Vallat.etal2009})
    \end{itemize}
\end{enumerate}

\section[CrPCS Alignment from Literature]{CrPCS Alignment from \citeauthor{Vivares.etal2005} \autocite{Vivares.etal2005}}
\label{sec:appendix_crpcs_alignment_from_vivares.etal2005}

This is the initial alignment which was used to as a starting point for the manual modeling process and subsequently refined.
As described in \cref{fig:results_manual_alignment}, an orange background indicates residues in the active site, and gray frames mark residues at the putative dimeric interface.
Light gray regions in the \ac{CrPCS} sequence were cut out from the resulting model as they had to be modeled \abinitio{}, light gray regions in the \ac{NsPCS} sequence were not present in the \pdbref{2BU3} template.
The locations of these residues are taken from \citeauthor{Vivares.etal2005} \autocite{Vivares.etal2005}.

\input{img/monomer-aln-long.texshade}

\section[Full Modeling Alignment]{Merged Manual and Automatically Created Alignments}
\label{sec:appendix_merged_manual_and_automatically_created_alignments}

This is the full alignment whose fingerprint was presented in \cref{fig:results_manual_and_server_generated_alignments}.
This alignment is used to compare the accuracy of the automated alignment algorithms used by the different modeling servers.
Some of the servers use several alignments and weigh them according to predicted accuracy;
in those cases, all alignments are reproduced here in order of precedence.\todo{Is that the right word?}
As in the previous section, an orange background in the \enquote{manual} sequence marks active site residues, and a green background marks the B-loops.

\input{img/server-manual-aln-long.texshade}

\section[Modeling Scores]{\score{Dope}, \score{Select}pro, and \score{Qmean6} Z scores of All Models}
\label{sec:appendix_dope_selectpro_and_qmean6_z_scores_of_all_models}

% TODO:
% - Align on decimal point
\begin{center}
  \centering
  \libertineTableFigures
  \begin{longtabu}{lX[c]{S}X[c]{S}X[c]{S}}
    \toprule
    Model                & {\score{Dope}} & {\score{Select}pro} & {\score{Qmean6} Z-score} \\
    \midrule
    Manual-1             & -1.52  & 0.658       & -1.196           \\
    Manual-2             & -1.49  & 0.657       & -1.186           \\
    Manual-3             & -1.41  & 0.626       & -0.397           \\
    Manual-4             & -1.36  & 0.655       & -1.086           \\
    Geno3D-1             & -0.95  & 0.587       & -2.347           \\
    Geno3D-2             & -0.96  & 0.574       & -2.116           \\
    Geno3D-3             & -0.98  & 0.577       & -1.413           \\
    Geno3D-4             & -0.99  & 0.618       & -1.792           \\
    Geno3D-5             & -0.96  & 0.576       & -2.348           \\
    HHpred-myaln         & -0.20  & 0.282       & -1.772           \\
    HHpred-theiraln      & 0.67  & 0.291       & -2.105           \\
    \program{I-Tasser}-1 & -0.34  & 0.595       & -2.742           \\
    \program{I-Tasser}-2 & -0.43  & 0.595       & -2.501           \\
    \program{I-Tasser}-3 & -0.54  & 0.570       & -2.540           \\
    \program{I-Tasser}-4 & -0.42  & 0.602       & -2.263           \\
    \program{I-Tasser}-5 & -0.36  & 0.576       & -2.523           \\
    \program{Loopp}-1    & -1.20  & 0.644       & -0.654           \\
    \program{Loopp}-2    & -1.25  & 0.645       & -0.797           \\
    \program{Loopp}-3    & 2.20  & 0.208       & -5.045           \\
    \program{Multicom}   & 0.02  & 0.483       & -2.032           \\
    \Phyre-int           & -0.42  & 0.542       & -1.988           \\
    \Phyre-nor           & 0.28  & 0.589       & -1.361           \\
    \Phyre-o2o           & -0.51  & 0.631       & -1.102           \\
    RaptorX-1            & -1.17  & 0.641       & -1.012           \\
    RaptorX-2            & 1.32  & 0.044       & -2.766           \\
    RaptorX-3            & 1.15  & 0.004       & -3.608           \\
    RaptorX-4            & 1.14  & 0.273       & -3.660           \\
    RaptorX-5            & 0.81  & 0.368       & -2.942           \\
    SM-auto              & 0.37  & 0.489       & -4.007           \\
    SM-myaln             & -0.02  & 0.548       & -1.275           \\
    SM-theiraln          & -0.99  & 0.591       & -1.015           \\
    \bottomrule
  \end{longtabu}
\end{center}

\FloatBarrier

\section[Distances Between Solvent-Exposed Lysine Residue \Chemepsilon-Nitrogens]{Distances Between \Chemepsilon-Nitrogens of Solvent-Exposed Lysine Residues}
\label{sec:distances_between_chemepsilon_nitrogens_of_surface_facing_lysine_residues}

This section lists the distances of the \Chemepsilon-nitrogens of solvent-exposed lysine residues for all simulations as mean ± standard deviation.
All values are in \si{\angstrom}.

\subsection{Monomers}
\label{sub:appendix_crosslinking_monomers}

\begin{table}
  \centering
  \footnotesize
  \caption{Lysine distances in monomer \program{Gromacs} explicit solvent \acs*{MD}}
  \input{img/crosslinking_gem.textable}
\end{table}

\begin{table}
  \centering
  \footnotesize
  \caption{Lysine distances in monomer \program{Amber} explicit solvent \acs*{MD}}
  \input{img/crosslinking_aem.textable}
\end{table}

\begin{table}
  \centering
  \footnotesize
  \caption{Lysine distances in monomer \program{Amber} implicit solvent \acs*{MD}}
  \input{img/crosslinking_aim.textable}
\end{table}

\FloatBarrier

\subsection{Dimers}
\label{sub:appendix_crosslinking_dimers}

\begin{sidewaystable}
  \centering
  \footnotesize
  \caption{Lysine distances in dimer \program{Gromacs} explicit solvent \acs*{MD}}
  \input{img/crosslinking_ged.textable}
\end{sidewaystable}

\begin{sidewaystable}
  \centering
  \footnotesize
  \caption{Lysine distances in dimer \program{Amber} explicit solvent \acs*{MD}}
  \input{img/crosslinking_aed.textable}
\end{sidewaystable}

\begin{sidewaystable}
  \centering
  \footnotesize
  \caption{Lysine distances in dimer \program{Amber} implicit solvent \acs*{MD}}
  \input{img/crosslinking_aid.textable}
\end{sidewaystable}
