%! TEX root = ../main.tex

\chapter{Methods}
\label{cha:methods}

The Python programming language \autocite{Rossum} was used for scripting and related tasks.
The Matplotlib library was used for all plotting endeavours \autocite{Hunter2007}.
IPython \autocite{Perez.Granger2007} was used as a Python shell.
Pandas \autocite{McKinney2008}, SciPy and NumPy \autocite{Jones.etal2001} were used for data analysis.
Py\program{Mol} \autocite{Schroedinger2010} was used for structure visualization.
\program{Theseus} \autocite{Theobald.Wuttke2006,Theobald.Wuttke2008,Theobald.Steindel2012} and TMalign \autocite{Zhang.Skolnick2005} as well as TMalign's Py\program{Mol} plugin were used for structural superpositioning.

All modeling steps and \ac{MD} simulations were carried out on 16-core virtualized workstations with \SI{8}{GiB} RAM running Ubuntu GNU/Linux.

\section{Template Identification}
\label{sec:methods_template_identification}

To identify potential template structures, the HHsearch \autocite{Soeding2005} algorithm was used.
Searches were constructed over the \database{Cath} \autocite{Orengo.etal1997}, \database{PDB} \autocite{Bernstein.etal1977}, Pfam \autocite{Punta.etal2012}, \database{Scop} \autocite{Murzin.etal1995}, and \database{Superfamily} \autocite{Gough.etal2001} databases.
The initial \ac{MSA} was generated using the HHblits \autocite{Remmert.etal2012} algorithm.
Apart from the alterations described, default options were used; see \cref{tab:hhsearch_settings_for_template_identification}.

\begin{table}[htb]
  \centering
  \caption{HHsearch settings for template identification}
  \label{tab:hhsearch_settings_for_template_identification}
  \libertineTableFigures
  \begin{tabu}{lr}
    \toprule
    Setting & Value \\
    \midrule
    Max. \acs*{MSA} iterations & \num{3} \\
    Score secondary structure & Yes \\
    Alignment mode & local \\
    Realign with MAC algorithm & Yes \\
    E-value threshold for MSA generation & $10^{-3}$ \\
    Min. coverage of \acs*{MSA} hits & \num{20} \\
    Min. sequence identity of MSA hits with query & \num{0} \\
    MAC realignment threshold & \num{0.3} \\
    Compositional bias correction & Yes \\
    \bottomrule
  \end{tabu}
\end{table}

Additionally, manual database searches were conducted in the Pfam, \database{Cath}, \database{Superfamily}, and \database{Scop} databases.
However, no useful additional results could be found.

\section{Sequence Alignment}
\label{sec:methods_sequence_alignment}

The initial alignment between \acp{PCS} from several species was taken from \citeauthor{Vivares.etal2005} \cite[][fig. 3]{Vivares.etal2005}.
An initial \ac{MSA} between the target, the template, and other homologous PCS sequences was created using the \program{T-Coffee} \autocite{Notredame.etal2000,DiTommaso.etal2011} program.
The \program{T-Coffee} server uses multiple methods for alignment and combines their output;
for this project,  all multiple alignment methods (\texttt{sap\_pair}, \texttt{TMalign\_pair}, \texttt{mustang\_pair}, \texttt{pcma\_msa}, \texttt{mafft\_msa}, \texttt{clustalw\_msa}, \texttt{dialigntx\_msa}, \texttt{poa\_msa}, \texttt{muscale\_msa}, \texttt{probcons\_msa}, \texttt{t\_coffee\_msa}, \texttt{amap\_msa}, \texttt{kalign\_msa}, \texttt{fsa\_msa}, \texttt{mus4\_msa}, \texttt{best\_pair4prot}, \texttt{fast\_pair}, \texttt{clustalw\_pair}, \texttt{lalign\_id\_pair}, \texttt{slow\_pair}, \texttt{proba\_pair}) were used.
An initial alignment of the \ac{CrPCS} sequence to this \ac{MSA} was generated using \program{T-Coffee}.
The alignment was subsequently refined in JalView \autocite{Waterhouse.etal2009} based on the template's secondary structure and the target's predicted secondary structure.
For target secondary structure prediction, the Psi\program{Pred} \autocite{Jones1999a,Buchan.etal2010} was used;
to analyze the secondary structure of the template, the \program{Dssp} algorithm was used \autocite{Kabsch.Sander1983}.
To create the dimeric sequence alignment, the monomeric sequence alignment was expanded to the second chains of both \pdbref{2BTW} and \pdbref{2BU3}.

\section{Modeling}
\label{sec:methods_modeling}

The \program{Modeller} program \autocite{Sali.Blundell1993} was used to generate the decoys for the target structure.
For both monomer and dimer, \num{2000} decoys were generated and subsequently clustered\footnote{%
  The scripts used for model building and clustering can be found at \href{https://bitbucket.org/runiq/modeling-clustering}{bitbucket.org/runiq/modeling-clustering} and on the accompanying DVD.}
using the \cmd{ptraj} tool of \program{Amber} \autocite{Case.etal2012}.
The nonaligned N- and C-terminal residues (residues \numrange{1}{6} and \numrange{217}{250}) were left out of the clustering process as their large \ac{RMSD} values could lead to an adverse effect on the the clustering results by interfering with the \ac{RMSD} in the main part of the protein (see \cref{ssub:results_manually_created_alignment}).

To determine the number of clusters with the highest information content, the guidelines provided by \citeauthor{Shao.etal2007} were followed \autocite{Shao.etal2007}.
Refer to \cref{ssub:fundamentals_clustering_metrics,tab:methods_guidelines_for_evaluation_of_clustering_metrics} for guidelines regarding the clustering metrics.

\begin{table}[htb]
  \centering
  \caption{Guidelines for evaluation of clustering metrics}
  \label{tab:methods_guidelines_for_evaluation_of_clustering_metrics}
  \libertineTableFigures
  \begin{tabu}to \linewidth{lX}
    \toprule
    Metric & Criterion for best cluster number \\
    \midrule
    Critical distance & \enquote{Elbow} criterion: Abrupt change indicates optimal cluster count \\
    \acs*{pSF} & Maximum where cluster number is manageably small \\
    \acs*{DBI} & Minimum where cluster number is manageably small \\
    \acs*{SSR/SST} & \enquote{Elbow} criterion: Abrupt change indicates optimal cluster count \\
    \bottomrule
  \end{tabu}
\end{table}

After the number of clusters with the highest information content was determined, the best cluster was selected according to the criteria proposed by \citeauthor{Shortle.etal1998} \autocite{Shortle.etal1998}:
A large number of decoys in a cluster suggests a broad \enquote{valley} in the energy hyperplane which indicates the global minimum and thus the native structure.
Therefore, the number of decoys in a cluster was the main criterion.
However, if two clusters had a similar number of decoys, the average \ac{RMSD} of a cluster's decoys to its centroid was used as a secondary criterion.
The selected final structure was the cluster's representative, which is the decoy with the lowest \ac{RMSD} to the cluster's centroid.
In order to verify whether the representative structures were viable and in a near-native state, the scores described in the next \lcnamecref{sec:methods_structure_assessment} were used.

The \program{Apbs} program \autocite{Baker.etal2001,Holst2001,Bank.Holst2003} was used to calculate electrostatic potential surfaces in order to assess the dimeric interface.
\program{Pdb2pqr} \autocite{Dolinsky.etal2007,Dolinsky.etal2004} was used in conjunction with \program{Amber}'s \forcefield{ff99} \autocite{Wang.etal2000} to generate initial charges for atoms.

\section{Structure Assessment}
\label{sec:methods_structure_assessment}

To assess the created decoys and the models generated by the modeling servers, a number of web services were used:
The SwissModel structure assessment tool \autocite{Melo.Feytmans1998,Benkert.etal2009,Benkert.etal2009a}, which incorporates the \score{Qmean6} score \autocite{Benkert.etal2009} and the \score{Anolea} \autocite{Melo.Feytmans1998} and \score{Gromos96} \autocite{Gunsteren.etal1996} scores.

Additionally, models were assessed by \score{Dope} score \autocite{Shen.Sali2006}, \score{Select}Pro score \autocite{Randall.Baldi2008}, and in Ramachandran plots according to their $\Phi$ and $\Psi$ backbone angles using the \program{Rampage} server and the backbone angle data from \citeauthor{Lovell.etal2003} \autocite{Lovell.etal2003}.

\score{Dope} is a size-normalized \enquote{energy} score.
As such, lower values indicate lower-\enquote{energy} and therefore better models.
\score{Select}pro is size-normalzed as well and can be in the range of \numrange{0}{1}, where a score of 1 indicates a native model and a score of 0 a poor one.
The \score{Qmean6} Z-score is measured in standard deviations from the average \score{Qmean6} score of all native PDB entries of similar size---a positive score indicates an above-average model, a negative score indicates a model of less-than-average quality.
Due to the nature of the modeling process, a positive score generally occurs only with one or more high-quality template(s) and a similarly high-quality alignment.

\section{Molecular Dynamics}
\label{sec:methods_molecular_dynamics}

The \program{Amber} \autocite{Case.etal2012} and \program{Gromacs} \autocite{Berendsen.etal1995,Lindahl.etal2001,VanDerSpoel.etal2005,Hess.etal2008} \ac{MD} suites were used to perform \ac{MD} simulations.
Explicit solvent simulations were performed with both packages while simulations in implicit solvent were only carried out with \program{Amber};
the \forcefield{GROMOS96 53A6} force field does not supply the parameters necessary for implicit solvent simulations.
All simulations were carried out in a \SI{50}{\milli\Molar} \ch{NaCl} solution.

\Iac{MD} simulation consists of the following general steps:

\begin{enumerate}
  \item Solvent \ac{EM} (explicit solvent simulations only)
  \item Whole-system \ac{EM}
  \item Restrained $NVT$ equilibration to the target temperature
  \item Restrained $NPT$ equilibration to the target pressure
  \item Production run
\end{enumerate}

The basic parameters for these steps can be found in \cref{tab:methods_amber_simulation_parameters} (\program{Amber}) and \cref{tab:methods_gromacs_simulation_parameters} (\program{Gromacs}).

\subsection{Model Preparation}
\label{sub:methods_model_preparation}

After having obtained the models from the clustering process, they had to be prepared for the dynamics simulations.
The H++ server version 3.1 \autocite{Anandakrishnan.etal2012,Myers.etal2006,Gordon.etal2005} was used to set the protonation states of certain amino acids.
See \cref{tab:methods_h++_settings_used_for_protonation} for the settings used.

\begin{table}[!htb]
  \centering
  \caption[H++ Settings]{H++ settings used for protonation}
  \label{tab:methods_h++_settings_used_for_protonation}
  \libertineTableFigures
  \begin{tabu}{lr}
    \toprule
      Setting             & Value                 \\
    \midrule
      Salinity            & \SI{50}{\milli\Molar} \\
      Internal Dielectric & \num{4}                     \\
      External Dielectric & \num{80}                    \\
      pH                  & \num{7}                     \\
    \bottomrule
  \end{tabu}
\end{table}

\subsection{\program{Amber}}
\label{sub:methods_amber}

For the preparation and analysis of \program{Amber} \ac{MD} runs, \program{Amber}Tools version 13 was used.
The \ac{MD} runs themselves were carried out using \program{Amber} version 12.
All simulations were done in the \forcefield{ff03} force field \autocite{Duan.etal2003}.
The salt concentration in the simulation box was set to \SI{50}{\milli\Molar}; for explicit solvent simulations the number of ions to add was determined by using $n = c \cdot V \cdot N_A$ where $n$ is the number of ions to add, $c$ is the target concentration, $V$ is the box volume, and $N_A$ is the Avogadro constant.
The \program{Shake} algorithm was used to constrain H bonds.

\begin{table}[htb]
  \centering
  \caption[\program{Amber}]{General settings for an \program{Amber} \acs*{MD} simulation}
  \label{tab:methods_amber_simulation_parameters}
  \begin{tabu}{Xr}
    \toprule
    Parameter                     & Value                                      \\
    \midrule
    Timestep $\delta t$           & \SI{2}{\femto\second}                      \\
    Thermostat                    & Langevin \autocite{Allen.Tildesley1989} (equilibration)/Berendsen (production)    \\
    Barostat (explicit solvent only) & Berendsen \\
    $\tau_p$                      & \SI{1.0}{\per\pico\second}                 \\
    $\gamma_{LN}$                 & \num{2.0}                                      \\
    Target temperature            & \SI{300}{\kelvin}  \\
    Coulomb interactions cutoff                              & \SI{8}{\angstrom}                                   \\
    Van der Waals cutoff                                     & \SI{8}{\angstrom}                                   \\
    \bottomrule
  \end{tabu}
\end{table}

\paragraph{Energy Minimization}
\label{par:methods_amber_energy_minimization}

For explicit solvent simulations, the minimization was performed in two steps:
First the solvent alone was minimized with the solute being restrained, in order to remove \enquote{holes} in the solvent molecule distribution.
For the solvent-only minimization, restraints with a force constant of \SI{5}{kcal\per\mol\per\square\angstrom} were placed on the solute.
All solvent molecules as well as all counterions were minimized.
In a second step, a whole-system minimization step was carried out.
After \num{500} steps of steepest descent minimization, a conjugate gradients step was done in order to improve convergence.
The convergence criterion was \SI{e-4}{\kilo cal\per\mol\per\square\angstrom}.
The actual input files used in the simulation can be found on the accompanying DVD.

For energy minimization in implicit solvent simulations, the solvent minimization step could be omitted since no dedicated solvent molecules were present.

\subsubsection{Explicit Solvent Simulations}
\label{ssub:methods_amber_explicit_solvent_simulations}

Due to the concerns raised against simulations in implicit solvent \autocite{Zhou2003}, explicit solvent simulations were performed as well, using the TIP3P water model \autocite{Jorgensen.etal1983}.
All explicit solvent simulations were carried out in a system with periodic boundary conditions modeled by the particle-mesh Ewald approach \autocite{Darden.etal1993} and a nonbonded interactions cutoff of \SI{8.0}{\angstrom}.
The protonated solutes were placed in the middle of a truncated octahedral box, with the smallest distance to a box face being \SI{11.0}{\angstrom}.
The box volume for the monomeric CrPCS structure was \SI{283621.296}{\angstrom}, which meant that a total of \num{9} \ch{Na+} and \num{9} \ch{Cl-} atoms had to be added in order to achieve a salt concentration of \SI{50}{\milli\Molar}.
The dimer's box volume was \SI{549156.595}{\angstrom}, so \num{17} \ch{Na+} and \num{17} \ch{Cl-} ions had to be added to achieve a \SI{50}{\milli\Molar} salt concentration.

\paragraph{$NVT$ Equilibration}
\label{par:methods_amber_nvt_equilibration}

Equilibration to the desired temperature was carried out in six steps.
Starting from \SI{0}{\kelvin}, at each step, the temperature was raised by \SI{50}{\kelvin} and the simulation was performed for \SI{40}{\pico\second} so that the system would have enough time to equilibrate.
A plot of temperature vs.\ time was used to monitor whether the temperature had converged to its target value.
For this equilibration, a \SI{5.0}{\kilo cal\per\mol\per\square\angstrom} restraint force was placed on all backbone atoms. \\

\paragraph{$NPT$ Equilibration}
\label{par:methods_amber_npt_equilibration}

In a second step, the system's pressure was equilibrated.
The $NPT$ ensemble is possibly the most \enquote{realistic} one as it most closely mirrors lab conditions.
The restraint force on all backbone atoms was gradually lowered to \num{2.0}, \num{0.5}, and eventually \SI{0.1}{\kilo cal\per\mol\per\square\angstrom}.
Because the pressure generally equilibrates much slower than the temperature (cf. \cref{sub:fundamentals_simulations_under_constant_temperature_and_pressure}), each step was run for \SI{200}{\pico\second}.
The pressure equilibration process was monitored by plotting pressure over time.

\paragraph{Production Run}
\label{par:methods_amber_production_run}

All restraints were then removed from the system and the production run was carried out.
The parameters used here are essentially the same as those for the $NPT$ equilibration step except for the lack of restraints and the usage of the Berendsen thermostat instead of the Langevin thermostat.

\subsubsection{Implicit Solvent Simulations}
\label{ssub:methods_amber_implicit_solvent_simulations}

Simulations using implicit solvent were performed with the GB model by \citeauthor{Onufriev.etal2004} \autocite{Onufriev.etal2004}.
The van der Waals radii were adjusted accordingly to match the \cmd{bondi2} parameter set \autocite{Bondi1964}, and the cutoffs for nonbonded interactions and Born radius calculation were set to \SI{16}{\angstrom}.

For \ac{EM} parameters, see \cref{par:methods_amber_energy_minimization}.

\paragraph{$NVT$ Equilibration}
\label{par:methods_amber_nvt_equilibration_implicit}

The $NVT$ equilibration step is almost identical to the explicit solvent one (cf. \cref{par:methods_amber_nvt_equilibration}), except for the use of implicit solvent and a timestep $\delta t$ of \SI{0.5}{\femto\second}, as per convention in the lab.
A plot of temperature vs.\ time was used to see if the temperature had converged to its target value.

\paragraph{Restraint Release}
\label{par:methods_amber_restraint_release}

As implicit solvent simulations do not have a dedicated box around the system, $NPT$ equlibration is not necessary.
However, the restraints on the system were loosened gradually so as to not introduce instability into the system, in the same fashion as described for explicit solvent simulations (\cref{par:methods_amber_npt_equilibration}).

\paragraph{Production Run}
\label{par:methods_amber_production_run}

As soon as all restraints had been removed from the system, the production simulation started.
The parameters are identical to those used for explicit simulations (\cref{par:methods_amber_production_run}).

\subsection{\program{Gromacs}}
\label{sub:methods_gromacs}

\program{Gromacs} version 4.6.1 was used to prepare, run, and analyze the \ac{MD} runs.
The force field used in all \program{Gromacs} simulations was \forcefield{GROMOS96 53A6} \autocite{Oostenbrink.etal2004}.

The general steps performed were the same as those for the \forcefield{ff03} force field (cf. \cref{ssub:methods_amber_explicit_solvent_simulations}).
However, the values of some parameters (mainly the electrostatic and van der Waals interaction cutoffs) were chosen differently to the \program{Amber} parameters, according to best practices described in the \program{Gromacs} manual \autocite{Gunsteren.etal1996} and the \forcefield{GROMOS96 53A6} force field publication by \citeauthor{Oostenbrink.etal2004} \autocite{Oostenbrink.etal2004}.

\begin{table}[htb]
  \centering
  \caption[\program{Gromacs} simulation parameters]{General settings for a \program{Gromacs} \acs*{MD} simulation}
  \label{tab:methods_gromacs_simulation_parameters}
  \begin{tabu}{X[2,j]X[3,j]}
    \toprule
    Parameter                                                & Value                                          \\
    \midrule
    Timestep $\delta t$                                      & \SI{2}{\femto\second}                          \\
    Thermostat                                               & Velocity-Rescaling (equilibration, \autocite{Bussi.etal2007})/Berendsen (production)   \\
    $\tau$                                                   & $0.8$                                          \\
    Target temperature                                       & \SI{300}{\kelvin} \\
    Target pressure     & \SI{1.0}{bar}                       \\
    Barostat            & Parrinello-Rahman ($NPT$ step 2)/Berendsen ($NPT$ step 1 and production)                          \\
    Coupling type       & Isotropic                           \\
    $\tau_p$            & \SI{1.0}{\per\pico\second}          \\
    Compressibility     & \SI{4.5e-5}{\per bar}               \\
    Neighbor search algorithm                                & Grid                                           \\
    Neighbor list cutoff                                     & \SI{0.9}{nm}                                   \\
    Coulomb interactions cutoff                              & \SI{0.9}{nm}                                   \\
    Van der Waals cutoff                                     & \SI{1.4}{nm}                                   \\
    Coulomb interactions model                               & Particle-mesh Ewald \autocite{Darden.etal1993} \\
    Potential shift at cutoff for Coulomb interactions       & yes                                            \\
    Potential shift at cutoff for van der Waals interactions & yes                                            \\
    Dispersion correction term preserves\ldots               & \ldots energy and pressure                     \\
    \bottomrule
  \end{tabu}
\end{table}

\paragraph{Energy Minimization}
\label{par:methods_gromacs_energy_minimization}

\program{Gromacs} employs steepest descent and conjugate gradients optimization algorithms as well, but it handles them differently:
Every \num{1000} steps, a steepest descent step is included, the rest are conjugate gradients.
The minimization stops when either \num{5000} steps have been performed or when the algorithm converges with a maximum force smaller than \SI{0.01}{\kilo\joule\per\mol\per\nano\meter}.
The step size for the line search algorithm is \SI{0.1}{nm}.
A grid is constructed for the neighbor list search in order to speed up the simulation.
Both the neighbor list and the Coulomb interaction close-range cutoff are set to \SI{0.9}{nm}.

The Coulomb interactions are modeled using the \ac{PME} method of \citeauthor{Darden.etal1993} \autocite{Darden.etal1993}.
A potential shift is used for the cutoff so as to not introduce artifacts at the border between short- and long-range Coulomb interactions.
The van der Waals interactions are modeled using a simple cutoff of \SI{1.4}{nm} as they drop off quickly with $r^{-6}$, due to the Lennard-Jones potential used to model them.
A similar potential shift algorithm as for the Coulomb cutoff is used in order to avoid artifacts at the cutoff border.
Additionally, in order to correct for potential artifacts at the van der Waals cutoff, a dispersion correction term is introduced as suggested by \citeauthor{Shirts.etal2007} \autocite{Shirts.etal2007}.
As with the \program{Amber} explicit solvent simulations, first a solvent-only minimization step is carried out, with the solute restrained.

\paragraph{$NVT$ Equilibration}
\label{par:methods_gromacs_nvt_equilibration}

This step is carried out in three distinct parts (in contrast to \program{Amber}'s six);
it has been established that, for presumably stable solutes such as the one modeled in this project, this is sufficient.
At each step, the temperature was raised by \SI{100}{\kelvin}.
For the first $NVT$ step, velocities were generated from a Maxwell-Boltzmann distribution at \SI{1}{\kelvin}.
The p-\program{Lincs} algorithm \autocite{Hess.etal1997} was used to place constraints on all bonds which allowed the timestep to be raised to \SI{2}{fs}.
Every step was performed for \num{25000} timesteps à \SI{2}{fs}, which makes the entire $NVT$ equilibration take \SI{150}{ps}.
Again, restraints were placed on the solute.
A plot of temperature vs.\ time was used to see if the temperature had converged to its target value.
The neighbor list was updated every fifth timestep.

The thermostat used was a velocity-rescaling algorithm.
The coupling constant $\tau$ was set to \num{0.8} \autocite[383]{Leach2001}.
The velocity-rescaling group of thermostats are as stable as the Berendsen thermostat;
however, they don't oscillate once arriving at the target temperature.
Unlike the Berendsen thermostat, they also sample from the correct $NVT$ ensemble.

\paragraph{$NPT$ Equilibration}
\label{par:methods_gromacs_npt_equilibration}

After $NVT$ equilibration was carried out and had successfully converged to the target temperature, the system's pressure had to be equilibrated.
Because the pressure generally equilibrates much slower than the temperature (cf. \cref{sub:fundamentals_simulations_under_constant_temperature_and_pressure}), each step was run for an entire nanosecond.
Analogously to the $NVT$ equilibration process, a more robust velocity rescaling barostat was first used in order to move close to the target pressure value of \SI{1}{\bar}, with a $\tau_p$ constant of \SI{1.0}{\per\pico\second}.
Afterwards, the Parrinello-Rahman barostat \autocite{Parrinello.Rahman1981,Nose.Klein1983} was used to make sure the system also samples from the correct $NPT$ ensemble.
Apart from the use of a barostat and the length of the simulation, all other settings were the same as in the $NVT$ equilibration step.
The pressure equilibration process was again monitored by plotting pressure over time.

\paragraph{Production Run}
\label{par:methods_gromacs_paragraph nameproduction_run}

As with \program{Amber}, the removal of all restraints signals the start of the actual production run.
Additionally, in order to improve performance, the Berendsen thermostat was used instead of the velocity-rescaling algorithm.
Apart from these changes, the parameters for the production run were identical to those for the $NPT$ equilibration.

\FloatBarrier

\section{Investigation of \ch{Cd^2+} Binding Sites}
\label{sec:methods_investigation_of_cd2+_binding_sites}

\ch{Cd^2+} binding sites were investigated according to \citeauthor{Maier.etal2003} \autocite{Maier.etal2003} using the alignment from \citeauthor{Vivares.etal2005} \autocite{Vivares.etal2005}.

The putative binding sites found by \citeauthor{Maier.etal2003} were aligned to their corresponding residues in the target protein structure and checked for sequence and structure similarities.
It is known that heavy metal ions can have an impact on calcium homeostasis \autocite{Stohs.Bagchi1995} and can block \ch{Ca^2+} channels \autocite{Swandulla.Armstrong1989}.
Therefore, the \ch{Ca^2+} binding sites present in the templates \pdbref{2BTW} and \pdbref{2BU3} were checked in the same manner.
